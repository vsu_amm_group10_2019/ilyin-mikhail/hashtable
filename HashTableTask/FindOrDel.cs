﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTableTask
{
    public partial class FindOrDel : Form
    {

        public long PhoneNumber;
        public FindOrDel(string status)
        {
            InitializeComponent();
            if (status == "find")
            {
                Text = "Find";
                button.Text = "Find";
            }
            else
            {
                Text = "Delete";
                button.Text = "Delete";
            }

        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            try
            {
                PhoneNumber = long.Parse(textBox.Text);
                Close();
            }
            catch
            {
                MessageBox.Show("Поле пустое!");
            }
            
        }
    }
}
