﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTableTask
{
    class HashTable
    {
        private List<Human>[] Table; //Информационный массив
        private readonly int _size; //Размер массива (таблицы)
        public HashTable(int size) //конструктор
        {
            _size = size;
            Table = new List<Human>[size];
        }

        public bool Add(Human human)
        {
            long hash = Human.HashFunction(human.PhoneNumber);
            long index = hash % _size;
            if (Table[index] == null)
            {
                Table[index] = new List<Human>();
            }
            if (Find(human.PhoneNumber) != null)
            {
                return false;
            }
            Table[index].Add(human);
            return true;
        }

        public Human Find(long PhoneNumber)
        {
            long hash = Human.HashFunction(PhoneNumber);
            long index = hash % _size; // hash mod _size
            List<Human> cell = Table[index];
            if (cell != null)
            {
                for (int i = 0; i < cell.Count; i++) // for i := 0 to cell.Size -1 do
                {
                    Human elem = cell.ElementAt(i);
                    if (elem.PhoneNumber == PhoneNumber)
                    {
                        //выйти из функции, вернув указанное значение в качестве результата.
                        return elem;
                    }
                }

            }
            return null;
        }

        public List<Human> GetData()
        {
            List<Human> list = new List<Human>();

            for (int i = 0; i < _size; i++)
            {
                if (Table[i] != null && Table[i].Count > 0) //Находим непустой элемент массива, содержащий список
                {
                    for (int j = 0; j < Table[i].Count; j++)
                    {
                        list.Add(Table[i].ElementAt(j));
                    }
                }
            }
            return list;
        }

        public void ToGrid(DataGridView grid)
        {
            grid.Rows.Clear();
            grid.RowCount = 0;
            for (int i = 0; i < _size; i++)
            {
                if (Table[i] != null && Table[i].Count > 0) //Находим непустой элемент массива, содержащий список
                {
                    for (int j = 0; j < Table[i].Count; j++)                    {
                        Human elem = Table[i].ElementAt(j);
                        grid.RowCount++;
                        grid.Rows[grid.RowCount-1].Cells[0].Value = elem.FIO;
                        grid.Rows[grid.RowCount-1].Cells[1].Value = elem.PhoneNumber;
                        grid.Rows[grid.RowCount-1].Cells[2].Value = elem.Adress;
                    }
                }
            }
            
        }

        public void Clear()
        {
            foreach (var list in Table)
            {
                list?.Clear();
            }
        }

        public void Delete(long PhoneNumber)
        {
            long hash = Human.HashFunction(PhoneNumber);
            long index = hash % _size; // hash mod _size
            List<Human> cell = Table[index];
            try
            {
                for (int i = 0; i < cell.Count; i++)
                {
                    Human elem = cell.ElementAt(i);
                    if (elem.PhoneNumber == PhoneNumber)
                    {
                        cell.RemoveAt(i);
                    }
                }

            }
            catch
            {
                MessageBox.Show("Не удалось выполнить операцию!");
            }
        }

    }
}
