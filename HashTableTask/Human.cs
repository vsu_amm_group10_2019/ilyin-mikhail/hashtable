﻿using System;
namespace HashTableTask

{
    public class Human
    {
        public string FIO { get; set; }
        public long PhoneNumber { get; set; }
        public string Adress { get; set; }
        public static long HashFunction(long key)
        {
            string keyChars = key.ToString();
            long result = 0;
            foreach (char c in keyChars)
            {
                result += c;
            }
            result += key * 2;
            return result;
        }
        public void ShowOnGrid(System.Windows.Forms.DataGridView grid)
        {
            grid.Rows.Clear();
            grid.RowCount = 1;
            grid.Rows[grid.RowCount - 1].Cells[0].Value = FIO;
            grid.Rows[grid.RowCount - 1].Cells[1].Value = PhoneNumber;
            grid.Rows[grid.RowCount - 1].Cells[2].Value = Adress;
        }
        
    }
}
