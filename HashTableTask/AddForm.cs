﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTableTask
{

    
    public partial class AddForm : Form
    {
        public Human Human { get; } = new Human();

        public AddForm()
        {
            InitializeComponent();
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Human.FIO = textBoxFIO.Text;
            try
            {
                Human.PhoneNumber = long.Parse(textBoxPN.Text);
                Close();
            }
            catch
            {
                MessageBox.Show("Неверно указан номер телефона");
            }
            Human.Adress = textBoxAdress.Text;
            
        }
    }
}
